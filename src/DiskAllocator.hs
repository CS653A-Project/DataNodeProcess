module DiskAllocator (allocateDisk) where

import System.Process as SP
import System.Environment


allocateDisk :: String -> IO ()
allocateDisk size = do
    SP.callCommand "mkdir -p filesystem"
    SP.callCommand $ "dd if=/dev/zero of=filesystem/virtual.dsk bs=1M count=" ++ size
    SP.callCommand "mkfs -t ext4 filesystem/virtual.dsk"

main :: IO ()
main = runDiskAllocator

runDiskAllocator :: IO ()
runDiskAllocator = do
    args <- getArgs
    allocateDisk $ (\[x] -> x) args