{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

import Network.Socket
import GHC.Generics
import Data.ByteString.Lazy (ByteString)
import Data.ByteString.Lazy.Internal
import Data.Aeson 
import Control.Concurrent
import System.Environment
import System.DiskSpace

data QueryType = Update | Get deriving (Show, Generic)   -- Query Data Type for Client Query Type

instance ToJSON QueryType

data DataNodeQuery = DataNodeQuery {            -- JSON Data Type for Client Query
    queryType :: QueryType,
    ipAddress :: String,
    diskSpace :: Integer 
} deriving (Show, Generic)

instance ToJSON DataNodeQuery                 -- For parsing incoming query from String to DataNodeQuery

getLocalData :: IO DataNodeQuery
getLocalData = do
    args <- getArgs
    space <- let
        getPath :: [String] -> String
        getPath [_, _, _, path] = path
        in getAvailSpace $ getPath args
    return DataNodeQuery {queryType = Update, ipAddress = ip args, diskSpace = space}
        where
            ip [local, _, _, _] = local


-- Function to connect to server after every 3 seconds

connectToServer :: String -> Int -> IO ()
connectToServer ipAddress port = do
    addressInfo <- getAddrInfo Nothing (Just ipAddress) (Just $ show port)
    let serverAddress = head addressInfo
    newSocket <- socket (addrFamily serverAddress) Stream defaultProtocol
    connect newSocket (addrAddress serverAddress)
    talkToServer newSocket
    sClose newSocket
    threadDelay 3000000                 -- Delaying 3 seconds
    connectToServer ipAddress port

-- Function to send query to the server and get its reply

talkToServer :: Socket -> IO ()
talkToServer serverSocket = do
    localData <- getLocalData
    let msg = unpackChars $ encode localData
    send serverSocket msg
    putStrLn $ "Query Sent - " ++ msg
    receivedMsg <- recv serverSocket 256
    putStrLn $ "Server's reply - " ++ receivedMsg

-- Main Function

main :: IO ()
main = runDataNodeUpdates

runDataNodeUpdates :: IO ()
runDataNodeUpdates = do
    args <- getArgs
    connectToServer (getServerIP args) (getServerPort args)
    where
        getServerIP [_, ip, _, _] = ip
        getServerPort [_, _, port, _] = read port :: Int