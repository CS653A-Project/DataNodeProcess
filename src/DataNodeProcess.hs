{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

module DataNodeProcess where

import Database.HDBC
import Database.HDBC.Sqlite3 (connectSqlite3, Connection)
import Network.Socket
import Data.Binary as DB (Binary, decode, encode)
import Data.ByteString.Lazy (ByteString)
import Data.ByteString.Lazy.Internal as DBLI
import Data.ByteString.Internal as DBI
import Network.BSD
import Data.Aeson as DA
import Control.Applicative
import Control.Monad
import GHC.Generics
import Data.Maybe
import System.Environment

data Message = Message {
                    getIp :: Maybe String,
                    getCurrChunk :: String,
                    getNextChunk :: Maybe String,
                    getQueryType :: String
                } deriving (Show, Generic)
data QueryResult = QueryResult {
                    getValue :: Maybe String,
                    getIpList :: [String]
                } deriving (Show, Generic)

instance ToJSON QueryResult where
    toJSON (QueryResult getValue getIpList) = object [
                                                "value" .= getValue,
                                                "ipList" .= getIpList
                                                ]

instance FromJSON Message where
    parseJSON (Object v) = Message <$>
                            v .:? "ip" <*>
                            v .: "currChunk" <*>
                            v .:? "nextChunk" <*>
                            v .: "queryType"
    parseJSON _ = mzero

initDataBase :: IO Connection 
initDataBase = do
    dbConn <- connectSqlite3 "filetable.db"
    run dbConn "DROP TABLE IF EXISTS filetable" []
    run dbConn "CREATE TABLE filetable (currChunk VARCHAR(255) NOT NULL, nextChunk VARCHAR(255), ip1 VARCHAR(15) NULL, ip2 VARCHAR(15) NULL, ip3 VARCHAR(15) NULL)" []
    commit dbConn
    return dbConn

runDataNodeProcess :: String -> IO ()
runDataNodeProcess ip = do
    dbConn <- initDataBase
    startServer dbConn ip

startServer :: Connection -> String -> IO ()
startServer dbConn ip = do
    protocol <- protoNumber <$> getProtocolByName "TCP"
    sock <- socket AF_INET Stream 0
    setSocketOption sock ReuseAddr 1
    localhost <- inet_addr ip
    bindSocket sock (SockAddrInet 12346 localhost)
    listen sock 100
    putStrLn $ "Server running at " ++ ip ++ ":12346"
    listenLoop sock dbConn

listenLoop :: Socket -> Connection -> IO ()
listenLoop sock dbConn = do
    (conn, _) <- accept sock
    recvdRawData <- recv conn 1024
    print recvdRawData
    resp <- processData dbConn recvdRawData
    print resp
    send conn resp
    listenLoop sock dbConn

queryType :: Maybe Message -> String
queryType (Just msg) = getQueryType msg
queryType _ = "Error"

processData :: Connection -> String -> IO String
processData dbConn recvdData = do
    print decodedData
    case queryType decodedData of
        "insert" -> insertInto dbConn decodedData
        "retrieve" -> getInfo dbConn decodedData
        "delete" -> deleteInfo dbConn decodedData
        _ -> return "Error"
    where
        byteStringData :: DBLI.ByteString
        byteStringData = DBLI.packChars recvdData
        decodedData :: Maybe Message
        decodedData = DA.decode byteStringData

insertInto :: Connection -> Maybe Message -> IO String
insertInto dbConn (Just msg) = do
    res <- quickQuery' dbConn ("SELECT * from filetable where currChunk=\""++ getCurrChunk msg ++ "\"") []
    let 
        getEmpty :: [[SqlValue]] -> String
        getEmpty [[_, _, ip1, ip2, ip3]]
            | ip1 == SqlNull = "ip1"
            | ip2 == SqlNull = "ip2"
            | otherwise = "ip3"
        in if null res
            then run dbConn "INSERT INTO filetable (currChunk, nextChunk, ip1) VALUES (?, ?, ?)" [toSql (getCurrChunk msg), toSql (fromJust $ getNextChunk msg), toSql (fromJust $ getIp msg)]
            else run dbConn ("INSERT INTO filetable (currChunk, nextChunk, "++ getEmpty res ++") VALUES (?, ?, ?)") [toSql (getCurrChunk msg), toSql (fromJust $ getNextChunk msg), toSql (fromJust $ getIp msg)] 
    commit dbConn
    return "OK"

getInfo :: Connection -> Maybe Message -> IO String
getInfo dbConn (Just msg) = do
    res <- quickQuery' dbConn ("SELECT * from filetable where currChunk=\""++ getCurrChunk msg ++ "\"") []
    let formattedData = let
                            result :: [[SqlValue]] -> QueryResult
                            result [[_, SqlByteString nextChunk, ip1, ip2, ip3]] = 
                                let 
                                    list :: [SqlValue] -> [String]
                                    list [] = []
                                    list (SqlNull:xs) = list xs
                                    list (SqlByteString x:xs) = DBI.unpackChars x : list xs 
                                    in QueryResult {getValue = Just $ DBI.unpackChars nextChunk, getIpList = list [ip1,ip2,ip3]}
                            result [] = QueryResult {getValue = Nothing, getIpList = []}
                            in result res
    return (DBLI.unpackChars $ DA.encode formattedData)

deleteInfo :: Connection -> Maybe Message -> IO String
deleteInfo dbConn maybeMsg = do
    res <- getInfo dbConn maybeMsg
    run dbConn ("DELETE from filetable where currChunk=\"" ++ getCurrChunk (fromJust maybeMsg) ++ "\"") []
    return res

main :: IO ()
main = do
    [ip] <- getArgs
    runDataNodeProcess ip